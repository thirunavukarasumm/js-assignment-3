function flatten(elements, depth) {
    if (elements.length === 0) {
        return [];
    } else {
        if (Array.isArray(elements)) {
            let result = [];
            if (depth === undefined) {
                depth = 1;
            }
            for (let index = 0; index < elements.length; index++) {
                if (Array.isArray(elements[index]) && depth !== 0) {
                    let flattenArray = flatten(elements[index], depth - 1);
                    for (let indexFlatten = 0; indexFlatten < flattenArray.length; indexFlatten++) {
                        result.push(flattenArray[indexFlatten]);
                    }
                } else if (elements[index] !== undefined) {

                    result.push(elements[index]);

                }
            }
            return result;
        } else {
            return 0;
        }

    }
}

module.exports = flatten;
