let filter = require('../filter.js')
const items = [1, 2, 3, 4, 5];

// console.log(items.filter(checkIfEven,items));
function checkIfEven(element, index){
    return element %2==0;
}
let result = filter(items,checkIfEven,items);

if(result === 0){
    console.log("Invalid input.");
}else{
    console.log(result);
}
