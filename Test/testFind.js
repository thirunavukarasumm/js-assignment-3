
let find = require('../find.js')

const items = [1, 2, 3, 4, 5];

function findCBFunction(element, index, array) {
    return element > 3;
}

let result = find(items, findCBFunction);

if (result == 0) {
    console.log("Invalid input.");
} else {
    console.log(result);
}