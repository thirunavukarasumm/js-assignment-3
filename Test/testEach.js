let each = require('../each.js')
const items = [1, 2, 3, 4, 5];


function printFunction(element, index, array) {
    console.log(`Element - ${element}, Index ${index}`);
}
let result = each(items, printFunction);

if (result == 0) {
    console.log("Invalid input.");
}
