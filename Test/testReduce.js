let reduce = require('../reduce.js')
const items = [1, 2, 3, 4, 5];

function reduceCBFunction(acc, element, index, array) {
    return acc + element;
}
let result = reduce(items, reduceCBFunction);

console.log(result);
