// Do NOT use .filter, to complete this function.
// Similar to `find` but you will return an array of all elements that passed the truth test
// Return an empty array if no elements pass the truth test

function filter(elements, cb) {

    if (Array.isArray(elements)) {
        if (elements.length === 0) {
            return [];
        } else {
            let resultList = [];
            let resultValue;
            for (let index = 0; index < elements.length; index++) {
                resultValue = cb(elements[index], index,elements)
                if (resultValue === true) {
                    resultList.push(elements[index]);
                }
            }
            if (resultList.length === 0) {
                return [];
            } else {
                return resultList;
            }

        }
    } else {
        return 0;
    }


}

module.exports = filter;