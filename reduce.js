// Do NOT use .reduce to complete this function.
// How reduce works: A reduce function combines all elements into a single value going from left to right.
// Elements will be passed one by one into `cb` along with the `startingValue`.
// `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
// `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.


function reduce(elements, cb, startingValue) {
    if (Array.isArray(elements)) {

        if (elements.length === 0 && startingValue === undefined) {
            return undefined;
        }
        if (elements.length === 0 && startingValue !== undefined) {
            return startingValue;
        } 
        if (elements.length === 0) {
            return [];
        } else {
            let index = 0;
            if (startingValue === undefined) {
                startingValue = elements[0];
                index = 1;
            }
            for (let iterator = index; iterator < elements.length; iterator++) {
                startingValue = cb(startingValue, elements[iterator], iterator, elements);

            }
            return startingValue;
        }

    } else {
        return 0;
    }



}
module.exports = reduce;