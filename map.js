// Do NOT use .map, to complete this function.
// How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
// Produces a new array of values by mapping each value in list through a transformation function (iteratee).
// Return the new array.


function map(elements, cb) {
    if (Array.isArray(elements)) {
        let resultList = [];

        if (elements.length === 0) {
            return [];
        } else {
            for (let index = 0; index < elements.length; index++) {
                let result = cb(elements[index], index, elements);
                resultList.push(result);
                

            }
            return resultList;
        }
    } else {
        return 0;
    }


}

module.exports = map;