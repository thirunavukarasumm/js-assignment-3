// Do NOT use .includes, to complete this function.
// Look through each value in `elements` and pass each element to `cb`.
// If `cb` returns `true` then return that element.
// Return `undefined` if no elements pass the truth test.

function find(elements, cb) {

    if (Array.isArray(elements)) {
        if (elements.length === 0) {
            return undefined;
        } else {
            for (let index = 0; index < elements.length; index++) {
                let result = cb(elements[index], index, elements);
                if (result) {
                    return elements[index];
                }

            }
            return undefined;
        }
    } else {
        return 0;
    }


}

module.exports = find;